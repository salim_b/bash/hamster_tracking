# Hamster tracking

Bash scripts and systemd service units to suspend or stop the Hamster time tracking [Flatpak app](https://github.com/projecthamster/hamster/pull/610#issuecomment-670926468).

This is meant as an alternative for Hamster's former GNOME idle detection which unfortunately [was removed in v3](https://github.com/projecthamster/hamster/issues/638).

## Installation

1.  Place the bash scripts in an appropriate place. We use `/usr/local/bin/hamster_tracking/` in the following:

    ```sh
    wget https://gitlab.com/salim_b/bash/hamster_tracking/-/archive/master/hamster_tracking-master.zip
    unzip hamster_tracking-master.zip
    cd hamster_tracking-master
    sudo mkdir /usr/local/bin/hamster_tracking
    sudo mv hamster_resume.sh hamster_stop.sh hamster_stop_resumable.sh hamster_suspend_on_idle.sh /usr/local/bin/hamster_tracking/
    ```

2.  Create an autostart entry for `hamster_suspend_on_idle.sh` (e.g. using [GNOME Tweaks](https://wiki.gnome.org/Apps/Tweaks), or if you're on Ubuntu, [`gnome-session-properties`](https://manpages.ubuntu.com/manpages/man1/gnome-session-properties.1.html)).

    A corresponding desktop file under `~/.config/autostart/` could have the following content:

    ```ini
    [Desktop Entry]
    Type=Application
    Exec="/usr/local/bin/hamster_tracking/hamster_suspend_on_idle.sh" 240 60
    Hidden=false
    NoDisplay=true
    StartupNotify=false
    X-GNOME-Autostart-enabled=true
    Name=Hamster: Suspend on idle
    Comment=Suspends Hamster time tracking when user is idle
    ```

3.  Adapt the systemd unit files `hamster_suspend.service` and `hamster_stop.service` to your needs, i.e.

    1. set the correct username in the `User=` key.

    2. replace `1000` with your actual user ID (UID) in the `Environment=` and `After=` keys.

4.  Copy the systemd unit files to `/etc/systemd/system/` and enable them:

    ```sh
    sudo mv systemd_units/hamster_suspend.service systemd_units/hamster_stop.service /etc/systemd/system/
    sudo systemctl enable hamster_suspend.service hamster_stop.service
    ```

3.  Reboot.

## Remaining problems

- Fix stopping Hamster on shutdown.

  Although the systemd unit `hamster_stop.service` seems to basically work – tested by a) manually starting and stopping it using `sudo systemctl start hamster_stop.service; sudo systemctl stop hamster_stop.service` and b) writing a string to file instead of running `hamster_stop.sh` in `ExecStop=` – neither `flatpak run org.gnome.Hamster stop` nor `notify-send` in `hamster_stop.sh` seem to work as supposed.

  The cause of Hamster not stopping is most probably that systemd already killed the process responsible for the Hamster Flatpak at the time `hamster_stop.service` is stopped. Statements from [2016](https://lists.freedesktop.org/archives/systemd-devel/2016-November/037916.html), [2017](https://bbs.archlinux.org/viewtopic.php?pid=1702855#p1702855) and even systemd author [Lennart Poettering himself in 2020](https://www.spinics.net/lists/systemd-devel/msg03591.html) seem to indicate this is impossible with current systemd... ☹
  
  The issue was reported as [#684](https://github.com/projecthamster/hamster/issues/684) in the upstream project. Additionally, we could try to ask for a solution on [Unix & Linux Stack Exchange](https://unix.stackexchange.com) or the [freedesktop.org devel mailing list](https://lists.freedesktop.org/archives/systemd-devel/), but the above information suggests this would most probably be an effort in vain.
  
  A completely different approach to achieve the same could consist of [simply overwriting](https://askubuntu.com/a/789027/622358) the system's `shutdown` command with a custom one that stops Hamster and then invokes the original shutdown command.
  
  Or another alternative might consist of a [systemd _user_ unit](https://wiki.archlinux.org/title/systemd/User), either depending on [Hamster's D-Bus service](https://github.com/projecthamster/hamster/wiki/Developers-corner#dbus-service) (unfortunately [largely undocumented](https://github.com/projecthamster/hamster/wiki/How-to-contribute#technical-documentation)) if the latter is actually started via systemd, or otherwise simply unconditionally stopping Hamster on exit (which would require that it be _started after_ the Hamster processes, unclear if this is possible). The latter would of course also stop Hamster on _reboot_, something not really desired but also not too bad, I think.

## Development

Feel free to [submit a pull request](https://gitlab.com/salim_b/bash/hamster_tracking/-/merge_requests/new) improving this little project.

### Useful systemd commands

- Edit an existing unit file: [`sudo systemctl edit --full hamster_stop.service`](https://www.freedesktop.org/software/systemd/man/systemctl.html#edit%20UNIT%E2%80%A6)

  This automatically reloads the systemd manager configuration ([`systemctl daemon-reload`](https://www.freedesktop.org/software/systemd/man/systemctl.html#daemon-reload)) afterwards. If you edit the `[Install]` section, you need to re-enable the service unit using `sudo systemctl reenable hamster_stop.service`.

- Manually run a unit file (e.g. to test it): `sudo systemctl start hamster_stop.service`

- Display a unit's status (e.g. after manually running it): `systemctl status hamster_stop.service`

- Display the systemd journal's last entries, augmented with explanation texts from the message catalog: `journalctl -xe`

### More information

- How to query Mutter for user session idle time is described in [this Stack Overflow answer](https://stackoverflow.com/a/46539896/7196903).

- A good and quick introduction into systemd is [provided by the Fedora project](https://docs.fedoraproject.org/de/quick-docs/understanding-and-administering-systemd/).

- Where to put systemd unit files is explained in [this Unix & Linux Stack Exchange answer](https://unix.stackexchange.com/a/367237/201803).

- The various systemd target units are documented in the [systemd.special](https://man.cx/systemd.special) manpage.

- The differences between executing a command locally and via systemd are explained in [this Unix & Linux Stack Exchange answer](https://unix.stackexchange.com/a/339645/201803).

- A way to detect whether or not a script is run from a systemd unit is presented in [this Server Fault answer](https://serverfault.com/a/927481/387031).

- Unfortunately, it's [not feasible](https://unix.stackexchange.com/a/174837/201803) to implement the Hamster suspend-resume and stop functionality using systemd _user_ units, so we must rely on _system_ units.

- [_Combined Suspend/resume service file_](https://wiki.archlinux.org/title/Power_management#Combined_Suspend/resume_service_file) in the ArchWiki.

- The environment variable `DISPLAY=:0` is required by Hamster and `DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/1000/bus` (UID) by Hamster as well as `notify-send` in order to work as supposed.

- It's not possible to set the service unit key [`Environment=`](https://www.freedesktop.org/software/systemd/man/systemd.exec.html#Environment=) completely dynamically because "the `$` character has no special meaning".

  [Specifier](https://www.freedesktop.org/software/systemd/man/systemd.unit.html#Specifiers) expansion might be an alternative in certain cases, but there's e.g. no specifier for the name or ID of the user set in the [`User=`](https://www.freedesktop.org/software/systemd/man/systemd.exec.html#User=) key.

- We have hardcoded the `DISPLAY` environment variable in the service unit to `Environment=DISPLAY=:0`, but it could also be dynamically set inside the bash scripts, see [this Stack Overflow answer](https://stackoverflow.com/a/66715739/7196903).

- Running something on shutdown _before_ systemd starts to kill a specific user process is tricky (before it starts to kill _any_ arbitrary user processes [currently even seems impossible](https://bbs.archlinux.org/viewtopic.php?pid=1702855#p1702855)). But at least the systemd unit belonging to a specific process ID (PID) can be determined using [`systemctl status <PID>`](https://www.freedesktop.org/software/systemd/man/systemctl.html#status%20PATTERN%E2%80%A6%7CPID%E2%80%A6%5D). Then this unit can be [configured as a reverse dependency](https://unix.stackexchange.com/a/41756/201803).

#!/bin/bash
# This script resumes the last running [Hamster](https://github.com/projecthamster/hamster) time tracker (Flatpak app) activity cached in
# `/tmp/last_hamster_activity.txt`.
#
# Requirements:
#
# - Unix tools (all available out-of-the-box in Ubuntu 20.04):
#   - `notify-send`

# abort if Hamster Flatpak is not available
if ! command -v flatpak >/dev/null || ! flatpak list --app | grep -q 'org\.gnome\.Hamster'; then
  echo "The Hamster Flatpak is required in order for this script to work, but is not installed. Aborting."
  exit 1
fi

# resume last Hamster activity cached in `/tmp/last_hamster_activity.txt`
if [[ `flatpak run org.gnome.Hamster current` == "No activity" ]] ; then
  
  if [[ -f /tmp/last_hamster_activity.txt ]] ; then
    last_activity=`head -n 1 /tmp/last_hamster_activity.txt`
  fi
  
  if [[ ! -z ${last_activity} ]] ; then
    flatpak run org.gnome.Hamster add "${last_activity}"
    notify-send --category=presence.online --icon=org.gnome.Hamster.GUI "${last_activity} resumed" "Resumed time tracking after system sleep."
  fi
fi

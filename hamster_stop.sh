#!/bin/bash
# This script simply halts the currently running [Hamster](https://github.com/projecthamster/hamster) time tracker (Flatpak app) activity.
#
# Requirements:
#
# - Unix tools (all available out-of-the-box in Ubuntu 20.04):
#   - `notify-send`

# abort if Hamster Flatpak is not available
if ! command -v flatpak >/dev/null || ! flatpak list --app | grep -q 'org\.gnome\.Hamster'; then
  echo "The Hamster Flatpak is required in order for this script to work, but is not installed. Aborting."
  exit 1
fi

# stop current Hamster activity
current_activity=`flatpak run org.gnome.Hamster current`

if [[ ${current_activity} != "No activity" ]] ; then
  current_activity=`echo ${current_activity} | grep -oP '\S+@\S+'`
  if [[ -f /tmp/last_hamster_activity.txt ]] ; then rm /tmp/last_hamster_activity.txt ; fi
  flatpak run org.gnome.Hamster stop
  notify-send --category=presence.offline --icon=org.gnome.Hamster.GUI "${current_activity} stopped" "Stopped time tracking at `date +"%T"` due to system shutdown."
fi

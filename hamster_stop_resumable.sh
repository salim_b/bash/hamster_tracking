#!/bin/bash
# This script halts the currently running [Hamster](https://github.com/projecthamster/hamster) time tracker (Flatpak app) activity and caches its ID to
# `/tmp/last_hamster_activity.txt` for later resumption.
#
# Requirements:
#
# - Unix tools (all available out-of-the-box in Ubuntu 20.04):
#   - `notify-send`

# abort if Hamster Flatpak is not available
if ! command -v flatpak >/dev/null || ! flatpak list --app | grep -q 'org\.gnome\.Hamster'; then
  echo "The Hamster Flatpak is required in order for this script to work, but is not installed. Aborting."
  exit 1
fi

# stop current Hamster activity and cache it to `/tmp/last_hamster_activity.txt`
current_activity=`flatpak run org.gnome.Hamster current`

if [[ ${current_activity} != "No activity" ]] ; then
  current_activity=`echo ${current_activity} | grep -oP '\S+@\S+'`
  echo $current_activity > /tmp/last_hamster_activity.txt
  flatpak run org.gnome.Hamster stop
  # COMMENTED OUT because notification would be received only after wake up and thereby confusing for the user
  # notify-send --category=presence.offline --icon=org.gnome.Hamster.GUI "${current_activity} stopped" "Stopped time tracking due to system sleep."
fi

#!/bin/bash
# This script regularly polls user session inactivity from GNOME's window manager [Mutter](https://en.wikipedia.org/wiki/Mutter_(software)), and if the
# specified timeout is reached, halts the currently running [Hamster](https://github.com/projecthamster/hamster) time tracker (Flatpak app) activity. The time
# tracking is resumed as soon as user session activity is detected again.
#
# Parameters:
#
# - `$1`: the user session inactivity threshold after which Hamster time tracking is automatically stopped in seconds; defaults to 4 mins (240s)
#
# - `$2`: the user session inactivity polling interval in seconds; the higher this is set, the more often the script will be run and thus produce system load;
#         defaults to 1 min (60s)
#
# Requirements:
#
# - Unix tools (all available out-of-the-box in Ubuntu 20.04):
#   - `bc`
#   - `gdbus`
#   - `notify-send`
#
# Notes:
#
# - Sometimes, querying the current Hamster activity using `flatpak run org.gnome.Hamster current` results in the following `flatpak-WARNING`:
#   
#   ```
#   Error writing credentials to socket: Error sending message: Broken pipe
#   ```
#
#   This seems to be an issue with the Hamster Flatpak itself, nothing we can do much about. It doesn't seem to negatively affect the proper functioning of this
#   script.

# set defaults if necessary
if [[ -z "$1" ]] ; then
  timeout=240000
else
  timeout=$(( $1*1000 ))
fi

if [[ -z "$2" ]] ; then
  polling_interval=60
else
  polling_interval="$2"
fi

# abort if Hamster Flatpak is not available
if ! command -v flatpak >/dev/null || ! flatpak list --app | grep -q 'org\.gnome\.Hamster'; then
  echo "The Hamster Flatpak is required in order for this script to work, but is not installed. Aborting."
  exit 1
fi

is_paused=false

# run infinite inactivity polling loop
while true; do
  
  inactivity=`gdbus call --session \
             --dest org.gnome.Shell \
             --object-path /org/gnome/Mutter/IdleMonitor/Core \
             --method org.gnome.Mutter.IdleMonitor.GetIdletime \
             | grep -oP '^(\(uint64\s*)\K\d+'`
  
  # resume time tracking if indicated
  if [[ ${is_paused} == "true" && ! -z "${current_activity}" && `flatpak run org.gnome.Hamster current` == "No activity" ]] ; then
    
    if [[ $inactivity -lt $timeout ]] ; then
      sleep 1
      flatpak run org.gnome.Hamster add "${current_activity}"
      notify-send --category=presence.online --icon=org.gnome.Hamster.GUI "${current_activity} resumed" "Resumed time tracking due to detection of user activity."
      is_paused=false
    fi
  
  # pause time tracking if indicated
  elif [[ $timeout -lt $inactivity ]] ; then
  
    current_activity=`flatpak run org.gnome.Hamster current`
    if [[ ${current_activity} != "No activity" ]] ; then
      current_activity=`echo ${current_activity} | grep -oP '\S+@\S+'`
      flatpak run org.gnome.Hamster stop
      notify-send --category=presence.offline --icon=org.gnome.Hamster.GUI "${current_activity} stopped" "Stopped time tracking after `bc <<< \"scale=1; $inactivity / 60000\"` minutes without user activity."
      is_paused=true
    fi
    
  fi
  
  sleep ${polling_interval}
  
done
